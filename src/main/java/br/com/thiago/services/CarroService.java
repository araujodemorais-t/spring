package br.com.thiago.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.thiago.models.Carro;


public interface CarroService {

	List<Carro> getListOfAllCarros();

}
