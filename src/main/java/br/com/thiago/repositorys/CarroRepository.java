package br.com.thiago.repositorys;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.thiago.models.Carro;

@Repository
public interface CarroRepository extends CrudRepository<Carro, Long> {

}
