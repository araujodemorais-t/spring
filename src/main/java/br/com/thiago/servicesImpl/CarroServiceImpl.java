package br.com.thiago.servicesImpl;

import java.util.List;

import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.thiago.models.Carro;
import br.com.thiago.repositorys.CarroRepository;
import br.com.thiago.services.CarroService;

@Transactional
@Service
public class CarroServiceImpl implements CarroService {

	@Autowired
	CarroRepository carroRepository;
	
	@Override
	public List<Carro> getListOfAllCarros() {
		List<Carro> carros = (List<Carro>) carroRepository.findAll();
		return carros;
	}

}
